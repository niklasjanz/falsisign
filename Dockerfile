FROM ubuntu:latest
WORKDIR /app
ENV DEBIAN_FRONTEND=noninteractive

# Fetch dependencies
RUN apt update && apt install -y poppler-utils imagemagick coreutils git python3 make

# Make python3 available as /usr/local/bin/python
RUN update-alternatives --install /usr/local/bin/python python /usr/bin/python3 3

RUN sed -i 's#<policy domain="coder" rights="none" pattern="PS" />##' /etc/ImageMagick-6/policy.xml && \
    sed -i 's#<policy domain="coder" rights="none" pattern="PS2" />##' /etc/ImageMagick-6/policy.xml && \
    sed -i 's#<policy domain="coder" rights="none" pattern="PS3" />##' /etc/ImageMagick-6/policy.xml && \
    sed -i 's#<policy domain="coder" rights="none" pattern="EPS" />##' /etc/ImageMagick-6/policy.xml && \
    sed -i 's#<policy domain="coder" rights="none" pattern="PDF" />##' /etc/ImageMagick-6/policy.xml && \
    sed -i 's#<policy domain="coder" rights="none" pattern="XPS" />##' /etc/ImageMagick-6/policy.xml

COPY . /app
